# 走近南荣相如
## 简介
大家好，我是南荣相如。主要从事.NET后端开发工作。喜欢看小说，玩游戏，所以对机器人写作、人工智能写作、文本生成、叙事生成等技术特别感兴趣。

## 联系我
* 微信公众号：南荣相如谈编程
* 头条号：[南荣相如谈编程](http://www.toutiao.com/c/user/102425115737/)
* 网站：https://www.aidaibi.net
* 邮箱：1282301776@qq.com
* Github: https://github.com/zcqiand
* 知乎：https://www.zhihu.com/people/zcqiand
* 博客: https://www.cnblogs.com/zcqiand/

## 捐赠支持
项目的发展离不开你的支持，如果觉得我们的内容对于你有所帮助，请作者喝杯咖啡吧！ 后续会继续完善更新！一起加油！

![](https://gitee.com/zcqiand/self-media/raw/master/assets/img/zhifu.png)

## 微信公众号
如果大家想要实时关注我更新的文章以及分享的干货的话，可以关注我的公众号。

![](https://gitee.com/zcqiand/self-media/raw/master/assets/img/weixin.jpg)

